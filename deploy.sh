#!/bin/ash
set -euo pipefail

for rootfs_file in "$@"; do
	variant=$(echo "$rootfs_file" | cut -d- -f2)
	version=$(echo "$rootfs_file" | cut -d- -f3)
	image_tag="$CI_REGISTRY_IMAGE/$variant:$version"

	echo "> Creating image $image_tag" >&2

	ctr=$(buildah from "tarball:$rootfs_file")
	buildah config \
		--cmd '/bin/sh' \
		--entrypoint '/bin/sh -l' \
		"$ctr"
	buildah commit "$ctr" "$image_tag"

	echo "> Pushing image $image_tag to the registry" >&2
	buildah push "$image_tag"

	if [ "v$version" = "$LATEST_STABLE" ]; then
		image_tag2="$CI_REGISTRY_IMAGE/$variant:latest"
		buildah tag "$image_tag" "$image_tag2"

		echo "> Pushing image $image_tag2 to the registry" >&2
		buildah push "$image_tag2"
	fi
done
