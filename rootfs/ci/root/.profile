export XDG_CACHE_HOME="${CI_PROJECT_DIR:-$HOME}/.cache"

# Allow pip to install packages into /usr/lib/python3.
# See https://gitlab.alpinelinux.org/alpine/aports/-/issues/16108
export PIP_BREAK_SYSTEM_PACKAGES=1
